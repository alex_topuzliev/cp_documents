<div class="hidden" id="content_cp_documents">
    
    {if $documents}

    <div class="table-wrapper-responsive">
        <table width="100%" class="table table-middle table-responsive">
        <thead>
            <tr>
                <th>
                    <a class="cm-ajax" href="{"`$c_url`&sort_by=date&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("date")}{if $search.sort_by == "date"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}
                </th>
                <th>                    
                    <a class="cm-ajax" href="{"`$c_url`&sort_by=name&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("document_name")}{if $search.sort_by == "name"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}
                </th>
                <th width="10%">
                    <a class="cm-ajax" href="{"`$c_url`&sort_by=category&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("document_category")}{if $search.sort_by == "category"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}
                </th>
                <th>
                    <a class="cm-ajax" href="{"`$c_url`&sort_by=type&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("document_type")}{if $search.sort_by == "type"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}
                </th>
                <th width="10%"></th>
                <th class="right">
                    <a class="cm-ajax" href="{"`$c_url`&sort_by=status&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("status")}{if $search.sort_by == "status"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}
                </th>
            </tr>
        </thead>
        
        <tbody class="cm-row-item">
            {foreach from=$documents item=document}
                <tr class="cm-row-status-{$request.status|lower}">
                    <td>{$document.timestamp|date_format:"`$settings.Appearance.date_format`, `$settings.Appearance.time_format`"}</td>
                    <td>
                        <a href="{"cp_documents.update&document_id=`$document.document_id`"|fn_url}">{$document.name}</a>
                    </td>
                    <td>{$document.category}</td>
                    <td>
                        {if $document.type == 'I'}{__("internal")}{/if}
                        {if $document.type == 'A'}{__("for_all")}{/if}
                    </td>
                    
                    <td> 
                        {capture name="tools_list"}
                            <li>{btn type="list" text=__("edit") href="cp_documents.update&document_id=`$document.document_id`"}</li>
                            <li>{btn type="list" text=__("delete") class="" href="cp_documents.delete&document_id=`$document.document_id`" method="POST"}</li>
                        {/capture}
                        <div class="hidden-tools">
                            {dropdown content=$smarty.capture.tools_list}
                        </div>
                    </td>

                    <td class="right nowrap" data-th="{__("status")}">
                        {include file="common/select_popup.tpl" popup_additional_class="dropleft" id=$document.document_id status=$document.status hidden=true object_id_name="document_id" table="cp_documents" update_controller="cp_documents"}
                    </td>
                    
                </tr>
            {/foreach}
        </tbody>
  
        </table>
    </div>

    {else}
        <p class="no-items">{__("no_data")}</p>
    {/if}
</div>