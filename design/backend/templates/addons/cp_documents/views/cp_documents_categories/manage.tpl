{capture name="mainbox"}

    <form action="{""|fn_url}" method="post" name="manage_cp_documents_form" class="form-horizontal form-edit cm-ajax">
    <input type="hidden" name="result_ids" value="pagination_contents,tools_cp_documents_buttons" />

    {include file="common/pagination.tpl" save_current_page=true save_current_url=true div_id=$smarty.request.content_id}

    {assign var="return_url" value=$config.current_url|escape:"url"}
    {assign var="c_url" value=$config.current_url|fn_query_remove:"sort_by":"sort_order"}

    {assign var="rev" value=$smarty.request.content_id|default:"pagination_contents"}
    {assign var="c_icon" value="<i class=\"icon-`$search.sort_order_rev`\"></i>"}
    {assign var="c_dummy" value="<i class=\"icon-dummy\"></i>"}

    {if $categories_data}

    <div class="table-wrapper-responsive">
        <table width="100%" class="table table-middle table-responsive">
        <thead>
            <tr>
                <th>
                    <a class="cm-ajax" href="{"`$c_url`&sort_by=category&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("category")}{if $search.sort_by == "category"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}
                </th>
                <th>
                    <a class="cm-ajax" href="{"`$c_url`&sort_by=position&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("position")}{if $search.sort_by == "position"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}                    
                </th>
                <th>{__("documents")}</th> 
                <th></th> 
                <th class="right">
                    <a class="cm-ajax" href="{"`$c_url`&sort_by=status&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("status")}{if $search.sort_by == "status"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}
                </th>
            </tr>
        </thead>
        
        <tbody class="cm-row-item">
            {foreach from=$categories_data item=categories}
                <tr class="cm-row-status-{$request.status|lower}">
                    <td>{$categories.category}</td>
                    <td>
                        {$categories.position}
                    </td>

                    <td width="12%" class="center" data-th="{__("documents")}">
                        <a href="{"cp_documents.manage?category_id=`$categories.category_id`"|fn_url}" class="badge">{$categories.document_count}</a>
                    </td>

                    <td> 
                        {capture name="tools_list"}
                            <li>{btn type="list" text=__("edit") href="cp_documents_categories.update&category_id=`$categories.category_id`"}</li>
                            <li>{btn type="list" text=__("delete") class="" href="cp_documents_categories.delete&category_id=`$categories.category_id`" method="POST"}</li>
                        {/capture}
                        <div class="hidden-tools">
                            {dropdown content=$smarty.capture.tools_list}
                        </div>
                    </td>

                    <td class="right nowrap" data-th="{__("status")}">
                        {include file="common/select_popup.tpl" popup_additional_class="dropleft" id=$categories.category_id status=$categories.status hidden=true object_id_name="category_id" table="cp_documents_categories" update_controller="cp_documents_categories"}
                    </td>                 
                </tr>
            {/foreach}
        </tbody>
  
        </table>
    </div>

    {else}
        <p class="no-items">{__("no_data")}</p>
    {/if}

    {capture name="adv_buttons"}
        {capture name="tools_list"}
            <li>{btn type="list" text=__("add_category") href="cp_documents_categories.add"}</li>
        {/capture}
        {dropdown content=$smarty.capture.tools_list icon="icon-plus" no_caret=true placement="right"}
    {/capture}

    </form>
    {include file="common/pagination.tpl" div_id=$smarty.request.content_id}

{/capture}

{include file="common/mainbox.tpl" title=__("categories") content=$smarty.capture.mainbox buttons=$smarty.capture.buttons adv_buttons=$smarty.capture.adv_buttons sidebar=$smarty.capture.sidebar content_id="cp_documents"}
