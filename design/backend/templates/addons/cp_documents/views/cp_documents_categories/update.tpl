{if $language_direction == "rtl"}
    {$direction = "right"}
{else}
    {$direction = "left"}
{/if}

{if $id}
    {$view_uri = "categories.view?category_id=`$id`"|fn_get_preview_url:$category_data:$auth.user_id}
{/if}

{if $categories_data}
    {foreach from=$categories_data item=category}
        {assign var="category" value=$category}
        {assign var="id" value=$category.category_id}
    {/foreach}
{/if}

{capture name="mainbox"}

{$hide_inputs = ""|fn_check_form_permissions}

<form action="{""|fn_url}" method="post" name="document_category_update_form" class="form-horizontal form-edit" enctype="multipart/form-data">
<input type="hidden" name="fake" value="1" />
<input type="hidden" name="category_id" value="{$id}" />
<input type="hidden" name="selected_section" value="{$smarty.request.selected_section}" />

<div id="content_document_category">
    <div id="acc_information" class="collapsed in">
    <div class="control-group">
        <label for="elm_category_name" class="control-label cm-required">{__("name")}:</label>
        <div class="controls">
            <input type="text" name="category_data[category]" id="elm_category_name" size="55" value="{$category.category}" class="input-large" {if $is_trash}readonly="readonly"{/if} />
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="elm_category_descr">{__("description")}:</label>
        <div class="controls">
            <textarea id="elm_category_descr" name="category_data[description]" cols="55" rows="8" class="input-large cm-wysiwyg input-textarea-long">{$categories_data.description}</textarea>
            
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="elm_position">{__("position")}</label>
        <div class="controls">
            <input type="text" name="category_data[position]" id="elm_position" size="3" class="input-micro" value="{$category.position}" />
        </div>
    </div>

    {include file="common/select_status.tpl" input_name="category_data[status]" id="elm_documents_categories_status" obj=$category hidden=true}
</div>

{capture name="buttons"}
    {if $id}
        {include file="common/view_tools.tpl" url="categories.update?category_id="}

        {capture name="tools_list"}
            {hook name="categories:update_tools_list"}
                <li>{btn type="list" href="categories.add?parent_id=$id" text=__("add_subcategory")}</li>
                <li>{btn type="list" href="products.add?category_id=$id" text=__("add_product")}</li>
                <li>{btn type="list" target="_blank" text=__("preview") href=$view_uri}</li>
                <li class="divider"></li>
                <li>{btn type="list" href="products.manage?cid=$id" text=__("view_products")}</li>
                <li>{btn type="list" class="cm-confirm" text=__("delete_this_category") data=["data-ca-confirm-text" => "{__("category_deletion_side_effects")}"] href="categories.delete?category_id=`$id`" method="POST"}</li>
            {/hook}
        {/capture}
        {dropdown content=$smarty.capture.tools_list}
    {/if}
    {include file="buttons/save_cancel.tpl" but_role="submit-link" but_target_form="document_category_update_form" but_name="dispatch[cp_documents_categories.update]" save=$id}
{/capture}
</form>

{/capture}

{if !$id}
    {include
        file="common/mainbox.tpl"
        title=__("new_category")
        sidebar=$smarty.capture.sidebar
        sidebar_position="left"
        content=$smarty.capture.mainbox
        buttons=$smarty.capture.buttons
    }
{else}
    {include
        file="common/mainbox.tpl"
        sidebar=$smarty.capture.sidebar
        sidebar_position="left"
        title_start=__("editing_category")
        title_end=$category_data.category
        content=$smarty.capture.mainbox
        select_languages=true
        buttons=$smarty.capture.buttons
        adv_buttons=$smarty.capture.adv_buttons
    }
{/if}
