{capture name="mainbox"}
{capture name="tabsbox"}

<form action="{""|fn_url}" method="post" enctype="multipart/form-data" name="cp_documents_form" class="form-horizontal form-edit">
<input type="hidden" class="cm-no-hide-input" name="document_data[selected_section]" value="{$smarty.request.selected_section}" />
<input type="hidden" class="cm-no-hide-input" name="document_data[document_id]" value="{$document.document_id}" />

    <div class="tabs cm-j-tabs clear">
        <ul class="nav nav-tabs">
            <li id="general_settings" class="cm-js"><a>{__("general_settings")}</a></li>
            {if $document}<li id="attachments" class="cm-js active"><a>{__("attachments")}</a></li>{/if}
        </ul>
    </div>
        
    <div id="content_general_settings">
        <div class="control-group">
            <label class="control-label cm-required" for="document_name">{__("document_name")}</label>
            <div class="controls">
                <input type="text" name="document_data[name]" id="document_name" value="{$document.name}" size="40" class="input-large" />
            </div>
        </div>
    
        <div class="control-group">
            <label class="control-label cm-required" for="document_category">{__("document_category")}</label>
            <div class="controls">
                <select name="document_data[category_id]" id="categoty_id">
                    {foreach from=$categories_data item=category_data}
                        <option {if $document.category_id == $category_data.category_id}selected{/if} value="{$category_data.category_id}">{$category_data.category}</option>
                    {/foreach}
                </select>
            </div>
        </div>

        <div class="control-group cm-no-hide-input">
            <label class="control-label" for="elm_product_full_descr">{__("document_description")}:</label>
            <div class="controls">
                {include file="buttons/update_for_all.tpl" display=$show_update_for_all object_id="full_description" name="update_all_vendors[full_description]"}
                <textarea id="elm_document_full_descr"
                        name="document_data[description]"
                        cols="55"
                        rows="8"
                        class="cm-wysiwyg input-large"
                        data-ca-is-block-manager-enabled="{fn_check_view_permissions("block_manager.block_selection", "GET")|intval}"
                >{$document.description}</textarea>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label cm-required" for="document_type">{__("document_type")}</label>
            <div class="controls">
                <select name="document_data[type]" id="document_type">
                    <option {if $document.type == "A"}selected{/if} value="A">{__("for_all")}</option>
                    <option {if $document.type == "I"}selected{/if} value="I">{__("internal")}</option>
                </select>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label">{__("usergroups")}</label>
            <div class="controls">
                {include file="common/select_usergroups.tpl" id="elm_usergroup_`$id`" name="document_data[usergroup_ids]" usergroups=["type"=>"C", "status"=>["A", "H"]]|fn_get_usergroups usergroup_ids=$document.usergroup_ids input_extra="" list_mode=false}
            </div>
        </div>

        {include file="common/select_status.tpl" input_name="document_data[status]" id="elm_documents_status" obj=$document hidden=true} 

        {if $document}       
            <div class="control-group">    
                <label class="control-label cm-required" for="opener_picker">{__("user")}</label>
                
                <div class="controls">     
                    <p class="strong">
                        {$user_full_name = "`$document.firstname` `$document.lastname`"|trim}
                        {if $user_full_name}
                            {if $document.user_id}
                                <a href="{"profiles.update?user_id=`$document.user_id`&selected_section=cp_documents"|fn_url}">{$user_full_name}</a>
                            {/if}
                        {/if}
                    </p>
                </div>

                {if $document.user_id}
                <div class="enter-data controls">
                    {capture name="tools_list"} 
                        <li>{include file="pickers/users/picker.tpl" extra_var="cp_documents.update&document_id=`$document.document_id`" display="radio" but_text=__("choose_user")}</li>             
                        <li>{btn type="list" text=__("delete") class="" href="cp_documents.delete_user&document_id=`$document.document_id`" method="POST"}</li>
                    {/capture}
                    {dropdown content=$smarty.capture.tools_list}
                </div>
                {else}
                <div class="enter-data controls">
                    <div class="clearfix shift-button"> 
                        {include file="pickers/users/picker.tpl" extra_var="cp_documents.update&document_id=`$document.document_id`" display="radio" but_text=__("choose_user") no_container=true but_meta="btn"}         
                    </div>
                </div>
                {/if}           
            </div>    
        {/if}
    </div>
</form>

{if $document}       
    <div id="content_attachments">
        <div class="btn-toolbar clearfix">
            <div class="pull-left">
            {capture name="add_new_picker"}
                {include file="addons/cp_documents/views/cp_documents/components/attachments.tpl" attachment=[] object_id=$object_id object_type=$object_type}
            {/capture}
            {include file="common/popupbox.tpl" id="add_new_attachments_files" text=__("new_attachment") link_text=__("add_attachment") content=$smarty.capture.add_new_picker act="general" icon="icon-plus"}
            </div>
        </div>

        {if $attachments}
        <div class="table-responsive-wrapper">   
            <table class="table table-middle table--relative table-objects table-responsive table-responsive-w-titles">
                {foreach from=$attachments item="a"}
                        {capture name="object_group"} 
                            {include file="addons/cp_documents/views/cp_documents/components/attachments.tpl" attachment=$a object_id=$object_id object_type=$object_type hide_inputs=$hide_inputs}
                        {/capture}
                        {include type="hidden" file="common/object_group.tpl" content=$smarty.capture.object_group id=$a.attachment_id text=$a.filename object_id_name="attachment_id" table="cp_documents_attachments" href_delete="cp_documents.delete_attachments?attachment_id=`$a.attachment_id`&redirect_url=`$redirect_url`" delete_target_id="attachments_list" header_text="{__("editing_attachment")}: `$a.filename`" additional_class="cm-sortable-row cm-sortable-id-`$a.attachment_id`" id_prefix="_attachments_" prefix="attachments" hide_for_vendor=$hide_for_vendor skip_delete=$skip_delete no_table="true" nostatus="true" link_text=$edit_link_text  draggable=true}
                {/foreach}
            </table>    
        </div>
        {else}
            <p>{__("no_data")}</p>
        {/if}
    </div>
{/if}

{/capture}
{include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox group_name=$runtime.controller active_tab=$smarty.request.selected_section track=true}

{capture name="buttons"}
    {capture name="tools_list"}              
        <li>{btn type="list" target="_blank" text=__("preview") href="index.php?dispatch=cp_documents.document_details&document_id=`$document.document_id`"}</li>
    {/capture}
    {dropdown content=$smarty.capture.tools_list}

    {if $document}
        {include file="buttons/save.tpl" but_name="dispatch[cp_documents.update]" but_role="submit-link" but_target_form="cp_documents_form"}
    {else}    
        {include file="buttons/save.tpl" but_name="dispatch[cp_documents.add]" but_role="submit-link" but_target_form="cp_documents_form"}
    {/if}
{/capture}

{/capture}

{include file="common/mainbox.tpl" title="{if empty($document)}{__("create_document")}{else}{__("update_document")}{/if}" content=$smarty.capture.mainbox select_languages=true buttons=$smarty.capture.buttons sidebar=$smarty.capture.sidebar}
