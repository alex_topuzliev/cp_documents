{if $in_popup}
    <div class="adv-search">
    <div class="group">
{else}
    <div class="sidebar-row">
    <h6>{__("search")}</h6>
{/if}
<form name="search_form" action="{""|fn_url}" method="get" class="{$form_meta}">

{if $smarty.request.redirect_url}
<input type="hidden" name="redirect_url" value="{$smarty.request.redirect_url}" />
{/if}

{if $selected_section != ""}
<input type="hidden" id="selected_section" name="selected_section" value="{$selected_section}" />
{/if}

{if $search.user_type}
<input type="hidden" name="user_type" value="{$search.user_type}" />
{/if}

{if $put_request_vars}
    {array_to_fields data=$smarty.request skip=["cp_documents"] escape=["data_id"]}
{/if}

{capture name="simple_search"}
{$extra nofilter}

<div class="sidebar-field">
    <label for="elm_name">{__("document_name")}</label>
    <div class="break">
        <input type="text" name="name" id="elm_name" value="{$search.name}" />
    </div>
</div>

<div class="sidebar-field">
    <label for="elm_name">{__("filename")}</label>
    <div class="break">
        <input type="text" name="filename" id="elm_filename" value="{$search.filename}" />
    </div>
</div>

<div class="sidebar-field">
    <label for="elm_name">{__("document_category")}</label>
    <div class="break">
        <select name="category" id="elm_category">
            <option value="0">--</option>
            {foreach from=$categories_data item=data}
                <option value="{$data.category_id}" {if $search.category == $data.category_id}selected{/if}>{$data.category}</option>
            {/foreach}
              
        </select>
    </div>
</div>

{include file="common/period_selector.tpl" period=$period display="form"}

{/capture}

{$no_adv_link = "ULTIMATE"|fn_allowed_for}
{include file="common/advanced_search.tpl" simple_search=$smarty.capture.simple_search advanced_search=$smarty.capture.advanced_search dispatch=$dispatch view_type="users" in_popup=$in_popup}

</form>

{if $in_popup}
</div></div>
{else}
</div><hr>
{/if}
