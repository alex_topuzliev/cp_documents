<?php
/*****************************************************************************
*                                                        © 2013 Cart-Power   *
*           __   ______           __        ____                             *
*          / /  / ____/___ ______/ /_      / __ \____ _      _____  _____    *
*      __ / /  / /   / __ `/ ___/ __/_____/ /_/ / __ \ | /| / / _ \/ ___/    *
*     / // /  / /___/ /_/ / /  / /_/_____/ ____/ /_/ / |/ |/ /  __/ /        *
*    /_//_/   \____/\__,_/_/   \__/     /_/    \____/|__/|__/\___/_/         *
*                                                                            *
*                                                                            *
* -------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license *
* and  accept to the terms of the License Agreement can install and use this *
* program.                                                                   *
* -------------------------------------------------------------------------- *
* website: https://store.cart-power.com                                      *
* email:   sales@cart-power.com                                              *
******************************************************************************/

$schema['cp_documents_categories'] = array(
    'modes' => array( 
        'manage' => array(
            'permissions' => 'view_cp_documents'
        ),
        'update' => array(
            'permissions' => 'manage_cp_documents'
        ),
        'add' => array(
            'permissions' => 'manage_cp_documents'
        )
    ),
    'permissions' => array ('GET' => 'view_cp_documents', 'POST' => 'manage_cp_documents')
);

$schema['cp_documents'] = array(
    'modes' => array(
        'manage' => array(
            'permissions' => 'view_cp_documents'
        ),
        'add' => array(
            'permissions' => 'manage_cp_documents'
        ),
        'view' => array(
            'permissions' => 'view_cp_documents'
        ),
        'document_details' => array(
            'permissions' => 'view_cp_documents'
        )
    ),
    'permissions' => array ('GET' => 'manage_cp_documents', 'POST' => 'manage_cp_documents')
);

return $schema;
