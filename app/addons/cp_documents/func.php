<?php
/*****************************************************************************
*                                                        © 2019 Cart-Power   *
*           __   ______           __        ____                             *
*          / /  / ____/___ ______/ /_      / __ \____ _      _____  _____    *
*      __ / /  / /   / __ `/ ___/ __/_____/ /_/ / __ \ | /| / / _ \/ ___/    *
*     / // /  / /___/ /_/ / /  / /_/_____/ ____/ /_/ / |/ |/ /  __/ /        *
*    /_//_/   \____/\__,_/_/   \__/     /_/    \____/|__/|__/\___/_/         *
*                                                                            *
*                                                                            *
* -------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license *
* and  accept to the terms of the License Agreement can install and use this *
* program.                                                                   *
* -------------------------------------------------------------------------- *
* website: https://store.cart-power.com                                      *
* email:   sales@cart-power.com                                              *
******************************************************************************/

use Tygh\Storage;
use Tygh\Languages\Languages;
use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_cp_get_documents($params = array(), $lang_code = CART_LANGUAGE) 
{
    $params = array_merge(array(
        'items_per_page' => 0,
        'page' => 1,
    ), $params);

    $sortings = array (
        'date' => 'timestamp',
        'name' => 'name',
        'description' => 'description',
        'category' => 'category',
        'type' => 'type',
        'status' => 'status'
    );

    $sorting_str = db_sort($params, $sortings, 'date', 'desc');

    $condition = array();

    if (isset($params['name']) && fn_string_not_empty($params['name'])) {
        $params['name'] = trim($params['name']);
        $condition[] = db_quote("?:cp_documents.name LIKE ?l", '%' . $params['name'] . '%');
    }

    if (isset($params['filename']) && fn_string_not_empty($params['filename'])) {
       
        $params['filename'] = trim($params['filename']);
        $condition[] = db_quote("da.filename LIKE ?l", '%' . $params['filename'] . '%');
    }

    if (!empty($params['period'])) {
        list($params['time_from'], $params['time_to']) = fn_create_periods($params);
        $condition [] = db_quote("?:cp_documents.timestamp >= ?i AND ?:cp_documents.timestamp <= ?i", $params['time_from'], $params['time_to']);
    } 

    if (!empty($params['category'])) {
        $condition[] = db_quote("?:cp_documents.category_id = ?i", $params['category']);
    }

    if ($dispatch = 'cp_documents.manage') {
        $company_id = Registry::get('runtime.company_id');
        if ($company_id != 0){
            $condition[] = db_quote("?:cp_documents.company_id = ?i", $company_id);
        }
    }

    if (!empty($params['document_id'])) {
        $condition[] = db_quote("?:cp_documents.document_id = ?i", $params['document_id']);
    } 

    if (!empty($params['category_id'])) {
        $condition[] = db_quote("?:cp_documents.category_id = ?i", $params['category_id']);
    } 

    if (Registry::get('runtime.mode') == 'view' || Registry::get('runtime.mode') == 'document_details') {
        $condition[] = db_quote("?:cp_documents.type = ?s", 'A');
    
        if (Registry::get('runtime.mode') == 'document_details') {
            $a = array('A','H');
            $condition[] = db_quote("?:cp_documents.status IN (?a)", $a);
            
        } else{
            $condition[] = db_quote("?:cp_documents.status = ?s", 'A');
        }
        
        $auth = Tygh::$app['session']['auth'];
        $condition[] = "(" . fn_find_array_in_set($auth['usergroup_ids'], '?:cp_documents.usergroup_ids', true) . ")";

        if (!empty($params['user_id'])) {
            $a = array(0, $params['user_id']);
            $condition[] = db_quote( "?:cp_documents.user_id IN (?a)", $a);
        } else {
            $condition[] = db_quote( "?:cp_documents.user_id = ?i", 0);
        }  
    }



    if (!empty($params['dispatch']) && $params['dispatch'] == 'profiles.update' && !empty($params['user_id'])) {

        $condition[] = db_quote( "?:cp_documents.user_id = ?i", $params['user_id']);
    }

    $condition[] = db_quote("c.status = ?s", 'A');   
    
    $condition[] = db_quote("cd.lang_code = ?s", $lang_code);

    $condition_str = $condition ? (' WHERE ' . implode(' AND ', $condition)) : '';

    $join_str =  db_quote(" 
        LEFT JOIN ?:cp_documents_category_descriptions cd ON ?:cp_documents.category_id = cd.category_id
        LEFT JOIN ?:cp_documents_categories c ON ?:cp_documents.category_id = c.category_id
        LEFT JOIN ?:users u ON ?:cp_documents.user_id = u.user_id"
    );

    $fields = array(
        '?:cp_documents.*',
        'cd.category',
        'u.firstname',
        'u.lastname', 
    );

    $fields_str = implode(', ', $fields);

    $limit = '';
    if (empty($params['items_per_page'])) {
        $params['total_items'] = db_get_field("SELECT COUNT(?:cp_documents.document_id) FROM ?:cp_documents". $join_str . $condition_str);
        $limit = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
    }
    
    $documents = db_get_array(
        "SELECT " 
        . $fields_str 
        . " FROM ?:cp_documents" 
        . $join_str 
        . $condition_str 
        . $sorting_str 
        . $limit
    );

    return array($documents, $params);
}

function fn_cp_add_document($document_data) 
{
    if(!empty($document_data)) 
    {
        $default_data = array(
            'timestamp' => TIME,
            'company_id' => Registry::get('runtime.company_id')
        );

        $document_data['usergroup_ids'] = empty($document_data['usergroup_ids']) ? '0' : implode(',', $document_data['usergroup_ids']);

        $row = array_merge($default_data, $document_data);

        $document_id = db_query("REPLACE INTO ?:cp_documents ?e", $row);

        return $document_id;
    }
}

function fn_cp_update_document($document_data)
{   
    if (!empty($document_data['document_id'])) {
        $document_data['usergroup_ids'] = empty($document_data['usergroup_ids']) ? '0' : implode(',', $document_data['usergroup_ids']);    
        db_query("UPDATE ?:cp_documents SET ?u WHERE document_id = ?i", $document_data, $document_data['document_id'] );
    }
}

function fn_cp_delete_document($document_id)
{
    db_query("DELETE FROM ?:cp_documents WHERE document_id = ?i", $document_id);
    db_query("DELETE FROM ?:cp_documents_attachments WHERE document_id = ?i", $document_id);
    return;
}

function fn_cp_delete_document_user($document_id)
{
    db_query("UPDATE ?:cp_documents SET user_id = 0 WHERE document_id = ?i", $document_id);
}

function fn_cp_update_documents_attachments($attachment_data, $attachment_id, $files = null, $lang_code = DESCR_SL)
{   
    if ($files != null) {
        $uploaded_data = $files;
    } else {
        $uploaded_data = fn_filter_uploaded_data('attachment_files');
    }

    if (!empty($attachment_id)) {

        $rec = array (
            'position' => $attachment_data['position']
        );

        db_query("UPDATE ?:cp_documents_attachments SET ?u WHERE attachment_id = ?i", $rec, $attachment_id);

    } elseif (!empty($uploaded_data)) {
        
        $rec = array (
            'document_id' => $attachment_data['document_id'],
            'position' => $attachment_data['position'],
            'filename' => $uploaded_data[$attachment_id]['name'],  
            'filesize' => $uploaded_data[$attachment_id]['size'],       
        );

        $attachment_id = db_query("INSERT INTO ?:cp_documents_attachments ?e", $rec);

        $uploaded_data[$attachment_id] = $uploaded_data[0];
        unset($uploaded_data[0]);
    }

    if ($attachment_id && !empty($uploaded_data[$attachment_id]) && $uploaded_data[$attachment_id]['size']) {

        $directory = 'cp_documents/' . $attachment_id;

        $filename = $uploaded_data[$attachment_id]['name'];
        $old_filename = db_get_field("SELECT filename FROM ?:cp_documents_attachments WHERE attachment_id = ?i", $attachment_id);

        if ($old_filename) {
            Storage::instance('attachments')->delete($directory . '/' . $old_filename);
        }

        list($filesize, $filename) = Storage::instance('attachments')->put($directory . '/' . $filename, array(
            'file' => $uploaded_data[$attachment_id]['path']
        ));

        if ($filesize) {
            $filename = fn_basename($filename);
            db_query("UPDATE ?:cp_documents_attachments SET filename = ?s, filesize = ?i WHERE attachment_id = ?i", $filename, $filesize, $attachment_id);
        }
    }

    return $attachment_id;
}

function fn_cp_delete_documents_attachments($attachment_id)
{
    $filename = db_get_field("SELECT filename FROM ?:cp_documents_attachments WHERE attachment_id = ?i", $attachment_id);

    $object_type = 'cp_documents';
    $directory = $object_type . '/' . $attachment_id;
    
    Storage::instance('attachments')->delete($directory . '/' . $filename);

    db_query("DELETE FROM ?:cp_documents_attachments WHERE attachment_id IN (?n) ", $attachment_id);

    return true;
}

function fn_cp_get_document_attachments($document_id)
{
    return db_get_array("SELECT * FROM ?:cp_documents_attachments WHERE document_id = ?i", $document_id);
}

function fn_cp_get_document_file($attachment_id)
{
    $data = db_get_row("SELECT * FROM ?:cp_documents_attachments WHERE attachment_id = ?i", $attachment_id);
   
    if (empty($data)) {
        return false;
    }

    $attachment_obj = Storage::instance('attachments');

    $object_type = 'cp_documents';

    $attachment_filename = $object_type . '/' . $attachment_id . '/' . $data['filename']; 

    if (!$attachment_obj->isExist($attachment_filename)) {
        return false;
    } 

    $attachment_obj->get($attachment_filename);
    exit;
}


//---------------------------------------------------- For categories ---------------------------------------------------------------

function fn_cp_get_documents_categories($params, $lang_code = CART_LANGUAGE)
{
    $controller = Registry::get('runtime.controller');
    $company_id = Registry::get('runtime.company_id');

    $condition = array();

    if(Registry::get('runtime.controller') == 'cp_documents'){
        $condition[] = db_quote("?:cp_documents_categories.status = ?s", 'A');   
    }
    
    if (!empty($params['category_id'])){
        $condition[] = db_quote(" ?:cp_documents_categories.category_id = ?i", $params['category_id']);
    } 
        
    if ($company_id != 0){
        $condition[] = db_quote("?:cp_documents_categories.company_id = ?i", $company_id);
    }

    $condition[] = db_quote("?:cp_documents_category_descriptions.lang_code = ?s", $lang_code);

    $condition_str = $condition ? (' WHERE ' . implode(' AND ', $condition)) : '';
    
    $sortings = array (
        'category' => 'category',
        'position' => 'position',
        'status' => 'status'
    );

    $sorting_str = db_sort($params, $sortings, 'position', 'asc');

    $categories_data = db_get_array(
        "SELECT * FROM ?:cp_documents_categories LEFT JOIN ?:cp_documents_category_descriptions
        ON ?:cp_documents_category_descriptions.category_id = ?:cp_documents_categories.category_id"
        . $condition_str
        . $sorting_str
    );
    
    $i= 0;  
    $total = count($categories_data)-1; 
    while($i<=$total){
        foreach ($categories_data as $category) {
            $count = fn_cp_get_document_count($category['category_id']);
            $categories_data[$i++]['document_count']=$count;
        } 
    }
    
    return $categories_data;
}

function fn_cp_get_document_count($category_id)
{
    $count = db_get_field(
        "SELECT COUNT(?:cp_documents.category_id) FROM ?:cp_documents_categories 
        LEFT JOIN ?:cp_documents
        ON ?:cp_documents_categories.category_id = ?:cp_documents.category_id
        WHERE ?:cp_documents.category_id = ?i", $category_id    
    );

    return $count;
}

function fn_cp_update_document_category($category_data, $category_id, $lang_code = DESCR_SL) 
{ 
    $category_data['company_id'] = Registry::get('runtime.company_id');

    if (!empty($category_id)){
        
        db_query("UPDATE ?:cp_documents_categories SET ?u WHERE category_id = ?i", $category_data, $category_id);

        $category_data['lang_code'] = $lang_code;

        db_query("UPDATE ?:cp_documents_category_descriptions SET ?u WHERE category_id = ?i AND lang_code = ?s", $category_data, $category_id, $category_data['lang_code']);
    
    } else {
        $category_id = db_query("INSERT INTO ?:cp_documents_categories ?e", $category_data);
        
        foreach (Languages::getAll() as $lang_code => $v) {
            
            $category_data['category_id'] = $category_id;
            $category_data['lang_code'] = $lang_code;
            
            db_query("REPLACE INTO ?:cp_documents_category_descriptions ?e", $category_data);
        }
    }
}

function fn_cp_delete_categories($category_id)
{
    db_query("DELETE FROM ?:cp_documents_categories WHERE category_id = ?i", $category_id);
    db_query("DELETE FROM ?:cp_documents_category_descriptions WHERE category_id = ?i", $category_id);
}
