<?php
/*****************************************************************************
*                                                        © 2019 Cart-Power   *
*           __   ______           __        ____                             *
*          / /  / ____/___ ______/ /_      / __ \____ _      _____  _____    *
*      __ / /  / /   / __ `/ ___/ __/_____/ /_/ / __ \ | /| / / _ \/ ___/    *
*     / // /  / /___/ /_/ / /  / /_/_____/ ____/ /_/ / |/ |/ /  __/ /        *
*    /_//_/   \____/\__,_/_/   \__/     /_/    \____/|__/|__/\___/_/         *
*                                                                            *
*                                                                            *
* -------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license *
* and  accept to the terms of the License Agreement can install and use this *
* program.                                                                   *
* -------------------------------------------------------------------------- *
* website: https://store.cart-power.com                                      *
* email:   sales@cart-power.com                                              *
******************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    fn_trusted_vars (
        'document_data'
    );

    if ($mode == 'add') {
        if(!empty($_REQUEST['document_data'])){
            $document_data = $_REQUEST['document_data'];

            $document_id = fn_cp_add_document($document_data);
        }

        return array(CONTROLLER_STATUS_OK, 'cp_documents.update&document_id='.$document_id);
    }

    if ($mode == 'update') {

        if(!empty($_REQUEST['document_data'])){
            $document_data = $_REQUEST['document_data'];
            fn_cp_update_document($document_data); 
    
            return array(CONTROLLER_STATUS_OK, 'cp_documents.update&document_id='.$document_data['document_id']);
        }

        if (!empty($_REQUEST['selected_user_id'])) {

            $user_id = $_REQUEST['selected_user_id'];

            $document_data = array(
                'user_id' => $_REQUEST['selected_user_id'],
                'document_id' => $_REQUEST['document_id']
            );

            fn_cp_update_document($document_data);
            return array(CONTROLLER_STATUS_OK, 'cp_documents.update?document_id='.$document_data['document_id']);
        }

        if (!empty($_REQUEST['attachment_data'])) {         
            fn_cp_update_documents_attachments($_REQUEST['attachment_data'], $_REQUEST['attachment_id']);      
        }
    }
    
    if ($mode == 'delete') {
        
        $document_id = $_REQUEST['document_id'];

        fn_cp_delete_document($document_id);
         
        return array(CONTROLLER_STATUS_OK, 'cp_documents.manage');            
    }

    if ($mode == 'delete_user') {

        $document_id = $_REQUEST['document_id'];

        fn_cp_delete_document_user($document_id);
        
        return array(CONTROLLER_STATUS_OK, 'cp_documents.update?document_id='.$document_id); 
    }

    if ($mode == 'delete_attachments') {      
        
        $document_id = $_REQUEST['document_id'];
        
        fn_cp_delete_documents_attachments($_REQUEST['attachment_id']);

        return array(CONTROLLER_STATUS_OK, 'cp_documents.update?document_id='.$document_id); 
    }

    if ($mode == 'update_status') {
        fn_tools_update_status($_REQUEST);
        exit;
    }
}

if ($mode == 'manage') {
   
    $params = $_REQUEST;

    list($documents, $search) = fn_cp_get_documents($params);

    $categories_data = fn_cp_get_documents_categories($params);
    
    Tygh::$app['view']
        ->assign('documents', $documents)
        ->assign('search', $search)
        ->assign('categories_data', $categories_data);

} elseif ($mode == 'add') {

    if (empty($categories_data)){
        
        $params = $_REQUEST;
        $categories_data = fn_cp_get_documents_categories($params);
        Tygh::$app['view']->assign('categories_data', $categories_data);
    }

    if (!empty($document_id)){

        $params = array('document_id' => $document_id);

        list($documents, $search) = fn_cp_get_documents($params);

        foreach($documents as $document){
            Tygh::$app['view']->assign('document', $document );
        }
    
    }

    if (!empty($_REQUEST['selected_user_id'])) {
        $attachments = fn_cp_get_document_attachments($document_id);
        
        Tygh::$app['view']->assign('attachments', $attachments);
    }

} elseif ($mode == 'update') {
    
    if(!empty($_REQUEST['document_id'])){
        $document_id = $_REQUEST['document_id'];
    }

    if (empty($categories_data)){
        
        $params = $_REQUEST;

        $categories_data = fn_cp_get_documents_categories($params);
        Tygh::$app['view']->assign('categories_data', $categories_data);
    }

    if (!empty($document_id)){

        $params = array('document_id' => $document_id);

        list($documents, $search) = fn_cp_get_documents($params);

        foreach($documents as $document){           
            Tygh::$app['view']->assign('document', $document );
        }
   
        $attachments = fn_cp_get_document_attachments($document_id);
        
        Tygh::$app['view']->assign('attachments', $attachments);
    }

} elseif ($mode == 'getfile') {
    if (!empty($_REQUEST['attachment_id'])) {
        fn_cp_get_document_file($_REQUEST['attachment_id']);
    }
    exit;

} 