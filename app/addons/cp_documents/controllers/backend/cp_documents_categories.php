<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if ($mode == 'update') {

        fn_cp_update_document_category($_REQUEST['category_data'], $_REQUEST['category_id']);
    }

    if ($mode == 'delete') {

        fn_cp_delete_categories($_REQUEST['category_id']);    
    }

    if ($mode == 'update_status') {
        
        fn_tools_update_status($_REQUEST);
        exit;
    }

    return array(CONTROLLER_STATUS_OK, 'cp_documents_categories.manage');
}

if ($mode == 'update' || $mode == 'manage') {

    $params = $_REQUEST;
    $categories_data = fn_cp_get_documents_categories($params);

    Tygh::$app['view']->assign('categories_data', $categories_data);
}





