<?php
/*****************************************************************************
*                                                        © 2019 Cart-Power   *
*           __   ______           __        ____                             *
*          / /  / ____/___ ______/ /_      / __ \____ _      _____  _____    *
*      __ / /  / /   / __ `/ ___/ __/_____/ /_/ / __ \ | /| / / _ \/ ___/    *
*     / // /  / /___/ /_/ / /  / /_/_____/ ____/ /_/ / |/ |/ /  __/ /        *
*    /_//_/   \____/\__,_/_/   \__/     /_/    \____/|__/|__/\___/_/         *
*                                                                            *
*                                                                            *
* -------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license *
* and  accept to the terms of the License Agreement can install and use this *
* program.                                                                   *
* -------------------------------------------------------------------------- *
* website: https://store.cart-power.com                                      *
* email:   sales@cart-power.com                                              *
******************************************************************************/

fn_add_breadcrumb(__('cp_documents'), $mode == 'view' ? '' : "cp_documents.view");

if($mode == 'view'){

    $params = $_REQUEST;
    $params['user_id'] = Tygh::$app['session']['auth']['user_id'];

    list($documents, $search) = fn_cp_get_documents($params);

    $categories_data = fn_cp_get_documents_categories($params);
    
    Tygh::$app['view']
        ->assign('documents', $documents)
        ->assign('search', $search)
        ->assign('categories_data', $categories_data);
} 

if($mode == 'document_details') {
    fn_add_breadcrumb(__('document_details'), $mode == 'document_details' ? '' : "cp_documents.document_details");

    $params = $_REQUEST;
    $params['user_id'] = Tygh::$app['session']['auth']['user_id'];

    list($documents, $search) = fn_cp_get_documents($params);

    if(empty($documents)){
        return array(CONTROLLER_STATUS_NO_PAGE);
    }; 

    $categories_data = fn_cp_get_documents_categories($params);
    $attachments = fn_cp_get_document_attachments($params['document_id']);

    Tygh::$app['view']
        ->assign('documents', $documents)
        ->assign('search', $search)
        ->assign('categories_data', $categories_data)
        ->assign('attachments', $attachments);
    
} 

if ($mode == 'getfile') {
    
    if (!empty($_REQUEST['attachment_id'])) {
        fn_cp_get_document_file($_REQUEST['attachment_id']);
    }
    exit;
} 