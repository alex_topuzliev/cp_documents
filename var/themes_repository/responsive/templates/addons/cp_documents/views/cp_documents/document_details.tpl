<div class="ty-wysiwyg-content">

    {foreach from=$documents item=document}
    <h3>{$document.name}</h3>

    <div class="ty-blog">
        <div class="ty-blog__date">{$document.timestamp|date_format:"`$settings.Appearance.date_format`"}</div>
        <div class="ty-blog__author">{$document.firstname} {$document.lastname} </div>
    </div>

    <div class="ty-column2 ty-float-left">    
        <div class="row-fluid">
            <span class="ty-strong">{__("document_category")}:</span>
            <span>{$document.category}</span>
        </div>

        <div class="ty-blog__description row-fluid">
            {if $document.description}
                <p>{$document.description nofilter}</p>
            {else}
                <p>{__("no_data")}</p>
            {/if}
        </div>    
    </div>
    {/foreach}

    <div class="ty-column3 ty-float-right">
        <span class="ty-strong">{__("filename")}:</span>
        {if $attachments}
            {foreach from=$attachments item=a}
                <div class="row-fluid">
                    <a href="{"cp_documents.getfile?attachment_id=`$a.attachment_id`"|fn_url}">{$a.filename}</a> ({$a.filesize} Kb)
                </div>
            {/foreach}
        {else}
            <p>{__("no_data")}</p>
        {/if}

        
    </div>
</div>

