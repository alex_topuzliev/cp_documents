
{if $documents}

<input type="hidden" name="result_ids" value="pagination_contents,tools_cp_documents_buttons"/>
{include file="common/pagination.tpl" save_current_page=true save_current_url=true div_id=$smarty.request.content_id}

{assign var="return_url" value=$config.current_url|escape:"url"}
{assign var="c_url" value=$config.current_url|fn_query_remove:"sort_by":"sort_order"}

{assign var="rev" value=$smarty.request.content_id|default:"pagination_contents"}
{assign var="c_icon" value="<i class=\"icon-`$search.sort_order_rev`\"></i>"}
{assign var="c_dummy" value="<i class=\"icon-dummy\"></i>"}

<div>
    <div>
        <table class="ty-table">
        <thead>
            <tr>
                <th>
                    <a class="cm-ajax" href="{"`$c_url`&sort_by=date&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("creation_date")}{if $search.sort_by == "date"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}
                </th>
                <th>                    
                    <a class="cm-ajax" href="{"`$c_url`&sort_by=name&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("document_name")}{if $search.sort_by == "name"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}
                </th>
                <th width="10%">
                    <a class="cm-ajax" href="{"`$c_url`&sort_by=category&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("document_category")}{if $search.sort_by == "category"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}
                </th>
            </tr>
        </thead>
        
        <tbody>
            {foreach from=$documents item=document}
                <tr>
                    <td>{$document.timestamp|date_format:"`$settings.Appearance.date_format`, `$settings.Appearance.time_format`"}</td>
                    <td>
                        <a href="{"cp_documents.document_details&document_id=`$document.document_id`"|fn_url}">{$document.name}</a>
                    </td>
                    <td>{$document.category}</td>
                </tr>
            {/foreach}
        </tbody>
  
        </table>
    </div>
</div>
{else}
    <p class="ty-no-items">{__("no_data")}</p>
{/if}

{include file="common/pagination.tpl" div_id=$smarty.request.content_id}

{capture name="mainbox_title"}{__("documents")}{/capture}