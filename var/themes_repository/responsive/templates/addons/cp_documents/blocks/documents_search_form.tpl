{capture name="section"}

<form action="{""|fn_url}" class="ty-orders-search-options" name="documents_search_form" method="get">
    
    <div class="clearfix">
        {include file="common/period_selector.tpl" period=$search.period form_name="documents_search_form"}

        <div class="span4 ty-control-group">
            <label class="ty-control-group__title" for="elm_name">{__("document_name")}</label>
            <div class="ty-select-field">
                <input class="ty-search-form__input" type="text" name="name" id="elm_filename" value="{$search.name}" />
            </div>
        </div>

        <div class="span4 ty-control-group">
            <label class="ty-control-group__title" for="elm_name">{__("document_category")}</label>
            <div class="ty-select-field">
                <select class="ty-search-form__input"  name="category" id="elm_category">
                    <option value="0">--</option>
                    {foreach from=$categories_data item=data}
                        <option value="{$data.category_id}" {if $search.category == $data.category_id}selected{/if}>{$data.category}</option>
                    {/foreach}
                    
                </select>
            </div>
        </div>  
    </div>

    <div class="buttons-container ty-search-form__buttons-container">
        {include file="buttons/button.tpl" but_meta="ty-btn__secondary" but_text=__("search") but_name="dispatch[cp_documents.view]"}
    </div>
     
</form>

{/capture}
{include file="common/section.tpl" section_title=__("search_options") section_content=$smarty.capture.section class="ty-search-form"}